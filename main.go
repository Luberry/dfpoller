package main

import (
	"bytes"
	"net/http"
	"os/exec"
	"strconv"
	"strings"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/sirupsen/logrus"
)

type dfCollector struct {
	blocks     *prometheus.Desc
	usedBytes  *prometheus.Desc
	availBytes *prometheus.Desc
	usePCT     *prometheus.Desc
}

var varLabels = []string{
	"filesystem",
	"mountpoint",
}

const prefix = "dfpoller_"

func newDFCollector() *dfCollector {
	return &dfCollector{
		blocks: prometheus.NewDesc(prefix+"blocks",
			"count of 1k blocks",
			varLabels, nil,
		),
		usedBytes: prometheus.NewDesc(prefix+"used_bytes",
			"number of bytes in use",
			varLabels, nil,
		),
		availBytes: prometheus.NewDesc(prefix+"avail_bytes",
			"number of bytes available",
			varLabels, nil,
		),
		usePCT: prometheus.NewDesc(prefix+"use_pct",
			"percentage of storage used",
			varLabels, nil,
		),
	}
}

func (collector *dfCollector) Describe(ch chan<- *prometheus.Desc) {

	//Update this section with the each metric you create for a given collector
	ch <- collector.usedBytes
	ch <- collector.blocks
	ch <- collector.availBytes
	ch <- collector.usePCT

}
func formMetric(desc *prometheus.Desc, labels []string, val string, tp prometheus.ValueType) prometheus.Metric {
	v := strings.TrimSuffix(val, "%")
	f, err := strconv.ParseFloat(v, 64)
	if err != nil {
		logrus.WithField("val", v).WithError(err).Error("could not parse metric value")
	}
	return prometheus.MustNewConstMetric(desc, tp, f, labels...)

}

//Collect implements required collect function for all promehteus collectors
func (collector *dfCollector) Collect(ch chan<- prometheus.Metric) {
	var out bytes.Buffer
	cmd := exec.Command("/usr/bin/df")
	cmd.Stdout = &out
	if err := cmd.Run(); err != nil {
		logrus.WithError(err).Error("failed to run command")
		return
	}
	lines := strings.Split(out.String(), "\n")
	if len(lines) <= 1 {
		return
	}
	for _, line := range lines[1:] {
		fields := strings.Fields(line)
		if len(fields) != 6 {
			continue
		}
		labels := []string{
			fields[0],
			fields[5],
		}
		ch <- formMetric(collector.usePCT, labels, fields[4], prometheus.GaugeValue)
		ch <- formMetric(collector.availBytes, labels, fields[3], prometheus.GaugeValue)
		ch <- formMetric(collector.usedBytes, labels, fields[2], prometheus.GaugeValue)
		ch <- formMetric(collector.blocks, labels, fields[1], prometheus.GaugeValue)
	}
}

func main() {
	foo := newDFCollector()
	prometheus.MustRegister(foo)

	http.Handle("/metrics", promhttp.Handler())
	logrus.Fatal(http.ListenAndServe(":9101", nil))
}
